package ru.kopylov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.kopylov.tm.service.UserService;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("username", "");
        model.addAttribute("password", "");
        return "registration";
    }

    @PostMapping("/registration")
    public String registrationUser(
            @ModelAttribute("username") final String login,
            @ModelAttribute("password") final String password
    ) {
        userService.save(login, password);
        return "redirect:/";
    }

}
