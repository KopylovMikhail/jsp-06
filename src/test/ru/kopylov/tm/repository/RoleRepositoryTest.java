package ru.kopylov.tm.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kopylov.tm.config.JpaConfig;
import ru.kopylov.tm.entity.Role;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringJUnitConfig(JpaConfig.class)
public class RoleRepositoryTest {
    
    @Autowired
    private RoleRepository  roleRepository;

    @Test
    public void save() {
        final Role role = new Role();
        roleRepository.save(role);
        assertNotNull(roleRepository.findById(role.getId()));
        roleRepository.deleteById(role.getId());
    }

    @Test
    public void deleteById() {
        final Role role = new Role();
        roleRepository.save(role);
        assertNotNull(roleRepository.findById(role.getId()));
        roleRepository.deleteById(role.getId());
        assertEquals(roleRepository.findById(role.getId()), Optional.empty());
    }

}