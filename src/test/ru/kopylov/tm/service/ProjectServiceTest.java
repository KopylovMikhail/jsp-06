package ru.kopylov.tm.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kopylov.tm.api.service.IProjectService;
import ru.kopylov.tm.config.JpaConfig;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.testconfig.TestConfigService;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringJUnitConfig({JpaConfig.class, TestConfigService.class})
public class ProjectServiceTest {

    @Autowired
    private IProjectService projectService;

    @Test
    public void save() {
        final Project project = new Project();
        projectService.save(project);
        assertNotNull(projectService.findById(project.getId()));
        projectService.deleteById(project.getId());
    }

    @Test
    public void findById() {
        final Project project = new Project();
        projectService.save(project);
        assertNull(projectService.findById("nope"));
        assertNotNull(projectService.findById(project.getId()));
        projectService.deleteById(project.getId());
    }

    @Test
    public void deleteById() {
        final Project project = new Project();
        projectService.save(project);
        assertNotNull(projectService.findById(project.getId()));
        projectService.deleteById(project.getId());
        assertNull(projectService.findById(project.getId()));
    }

    @Test
    public void findAll() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.save(project1);
        projectService.save(project2);
        assertTrue(projectService.findAll().size() > 1);
        assertFalse(projectService.findAll().isEmpty());
        projectService.deleteById(project1.getId());
        projectService.deleteById(project2.getId());
    }

}