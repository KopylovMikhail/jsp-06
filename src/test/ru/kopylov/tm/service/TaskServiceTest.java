package ru.kopylov.tm.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kopylov.tm.api.service.ITaskService;
import ru.kopylov.tm.config.JpaConfig;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.testconfig.TestConfigService;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringJUnitConfig({JpaConfig.class, TestConfigService.class})
public class TaskServiceTest {
    
    @Autowired
    private ITaskService taskService;

    @Test
    public void save() {
        final Task task = new Task();
        taskService.save(task);
        assertNotNull(taskService.findById(task.getId()));
        taskService.deleteById(task.getId());
    }

    @Test
    public void findById() {
        final Task task = new Task();
        taskService.save(task);
        assertNull(taskService.findById("nope"));
        assertNotNull(taskService.findById(task.getId()));
        taskService.deleteById(task.getId());
    }

    @Test
    public void deleteById() {
        final Task task = new Task();
        taskService.save(task);
        assertNotNull(taskService.findById(task.getId()));
        taskService.deleteById(task.getId());
        assertNull(taskService.findById(task.getId()));
    }

    @Test
    public void findAll() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        taskService.save(task1);
        taskService.save(task2);
        assertTrue(taskService.findAll().size() > 1);
        assertFalse(taskService.findAll().isEmpty());
        taskService.deleteById(task1.getId());
        taskService.deleteById(task2.getId());
    }

}